<?php
        if ($row) {

        	$id_topic = $row->id_topic;
            $topic_name = $row->topic_name;
            $action = "ubah";

        } else {

        	$id_topic = 
            $topic_name =  "";
        	$action = "simpan";

        }
?>
<script>
    $(document).ready(function(){
        $(".simpan").click(function(){
            $("#formsubmit").trigger("submit");
            return false;
        });
        $(".batal").click(function(){
            window.location = "<?php echo site_url('topic');?>";
            return false;
        });
    });
</script>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-success">
            <div class="box-header with-border">
            </div>
            <?php echo form_open('topic/simpan_topic/'.$action,array("id"=>"formsubmit"));?>
            <input type="hidden" name="idlama" value="<?php echo $id_topic; ?>" />
            <div class="box-body">
                <div class="form-horizontal">
                    <!-- <div class="form-group">
                        <label class="col-sm-2 control-label">Kode Negara</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" required name="kode" value="<?php echo $kode; ?>" <?php echo $readonly; ?>/>
                        </div>
                    </div> -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Topic Name</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" required name="topic_name" value="<?php echo $topic_name; ?>" />
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="box-footer">
                <div class="pull-right">
                    <div class="btn-group">
                       <button type=submit class="simpan btn btn-primary btn-md" title="Add"><i class="fa fa-save"></i></button>
                       <button class="batal btn btn-danger btn-md" title="Batal"><i class="fa fa-times-circle"></i></button>
                    </div>
                </div>
            </div>
            <?php echo form_close();?>
        </div>
    <div>
</div>