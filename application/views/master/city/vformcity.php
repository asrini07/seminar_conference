<?php
        if ($row) {

        	$id_city = $row->id_city;
            $city_name = $row->city_name;
            $id_country = $row->id_country;
            $action = "ubah";

        } else {

        	$id_city = 
            $city_name = 
            $id_country = "";
        	$action = "simpan";

        }
?>
<script>
    $(document).ready(function(){
        $(".simpan").click(function(){
            $("#formsubmit").trigger("submit");
            return false;
        });
        $(".batal").click(function(){
            window.location = "<?php echo site_url('city');?>";
            return false;
        });
    });
</script>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-success">
            <div class="box-header with-border">
            </div>
            <?php echo form_open('city/simpan_city/'.$action,array("id"=>"formsubmit"));?>
            <input type="hidden" name="idlama" value="<?php echo $id_city; ?>" />
            <div class="box-body">
                <div class="form-horizontal">
                    <!-- <div class="form-group">
                        <label class="col-sm-2 control-label">Kode Negara</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" required name="kode" value="<?php echo $kode; ?>" <?php echo $readonly; ?>/>
                        </div>
                    </div> -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">City Name</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" required name="city_name" value="<?php echo $city_name; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Country</label>
                        <div class="col-sm-10">
                            <select name="id_country" class="form-control">
                                <?php
                                    foreach ($country->result() as $row1){
                                    if ($id_country == $row1->id_country) $selected = "selected"; else $selected = "";
                                        echo "<option value='".$row1->id_country."' ".$selected.">".$row1->country_name."</option>";
                                    }
                                ?>
                            </select>
                            <!-- <input class="form-control" type="text" required name="id_country" value="<?php // echo $id_country; ?>" /> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="pull-right">
                    <div class="btn-group">
                       <button type=submit class="simpan btn btn-primary btn-md" title="Add"><i class="fa fa-save"></i></button>
                       <button class="batal btn btn-danger btn-md" title="Batal"><i class="fa fa-times-circle"></i></button>
                    </div>
                </div>
            </div>
            <?php echo form_close();?>
        </div>
    <div>
</div>