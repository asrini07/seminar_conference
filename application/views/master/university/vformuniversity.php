<?php
        if ($row) {

        	$id_universitas = $row->id_universitas;
            $universitas_name = $row->universitas_name;
            $singkatan = $row->singkatan;
            $id_city = $row->id_city;
            $action = "ubah";

        } else {

        	$id_universitas = 
            $universitas_name = 
            $singkatan = 
            $id_city = "";
        	$action = "simpan";

        }
?>
<script>
    $(document).ready(function(){
        $(".simpan").click(function(){
            $("#formsubmit").trigger("submit");
            return false;
        });
        $(".batal").click(function(){
            window.location = "<?php echo site_url('university');?>";
            return false;
        });
    });
</script>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-success">
            <div class="box-header with-border">
            </div>
            <?php echo form_open('university/simpan_university/'.$action,array("id"=>"formsubmit"));?>
            <input type="hidden" name="idlama" value="<?php echo $id_universitas; ?>" />
            <div class="box-body">
                <div class="form-horizontal">
                    <!-- <div class="form-group">
                        <label class="col-sm-2 control-label">Kode Negara</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" required name="kode" value="<?php echo $kode; ?>" <?php echo $readonly; ?>/>
                        </div>
                    </div> -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">University Name</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" required name="universitas_name" value="<?php echo $universitas_name; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Abbreviation</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" required name="singkatan" value="<?php echo $singkatan; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">City</label>
                        <div class="col-sm-10">
                            <!-- <input class="form-control" type="text" required name="id_city" value="<?php echo $id_city; ?>" /> -->
                            <select name="id_city" class="form-control">
                                <?php
                                    foreach ($city->result() as $row1){
                                    if ($id_city == $row1->id_city) $selected = "selected"; else $selected = "";
                                        echo "<option value='".$row1->id_city."' ".$selected.">".$row1->city_name."</option>";
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="pull-right">
                    <div class="btn-group">
                       <button type=submit class="simpan btn btn-primary btn-md" title="Add"><i class="fa fa-save"></i></button>
                       <button class="batal btn btn-danger btn-md" title="Batal"><i class="fa fa-times-circle"></i></button>
                    </div>
                </div>
            </div>
            <?php echo form_close();?>
        </div>
    <div>
</div>