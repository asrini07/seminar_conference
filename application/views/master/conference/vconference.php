<script>
	$(document).ready(function() {
		// $('#myTable').fixedHeaderTable({ height: '500', altClass: 'odd', footer: true});
        $("table tr#data:first").addClass("bg-gray");
        $("table tr#data ").click(function(){
            $("table tr#data ").removeClass("bg-gray");
            $(this).addClass("bg-gray");
        });
		$(".simpan").click(function(){
			window.location="<?php echo site_url('conference/formconference');?>";
			return false;
		});
		$(".ubah").click(function(){
			var id= $(".bg-gray").attr("href");
			window.location="<?php echo site_url('conference/formconference');?>/"+id;
			return false;
		});
		$(".hapus").click(function(){
			var id=$(".bg-gray").attr("href");
			window.location="<?php echo site_url('conference/hapusconference');?>/"+id;
		});
	});
</script>
<?php
	if($this->session->flashdata('message')){
		$pesan=explode('-', $this->session->flashdata('message'));
		echo "<div class='alert alert-".$pesan[0]."' alert-dismissable>
		<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
		<b>".$pesan[1]."</b>
		</div>";
	}
?>
<div class="row">
	<div class="col-md-12">
		<div class="box box-success">
			<div class="box-header with-border">
			</div>
			<div class="box-body">
				<table id="myTable" class="table table-bordered table-hover">
					<thead>
						<tr class="bg-navy">
	                        <th width='10%'>No</th>
	                        <th width='15%'>Conference Code</th>
	                        <th width='40%'>Conference Name</th>
	                        <th width='40%'>Status</th>
	                    </tr>
					</thead>
					<tbody>
						<?php
							$i = 0;
		                    foreach ($row->result() as $row){
		                        $i++;
		                        echo "<tr id='data' href='".$row->id_conference."'>
		        						 <td>".$i."</td>
		                                 <td align=center>".$row->id_conference."</td>
		                                 <td>".$row->conference_name."</td>
		                                 <td>".$row->status."</td>
		                              </tr>";
		                    }
						?>
					</tbody>
				</table>
			</div>
			<div class="box-footer">
				<div class="btn-group pull-right">
                    <button class="simpan btn btn-primary"><i class="fa  fa-plus"></i></button>
                    <button class="ubah btn btn-warning"><i class="fa fa-pencil"></i></button>
                    <button class="hapus btn btn-danger" ><i class="fa  fa-times"></i></button>
				</div>
			</div>
		</div>
	</div>
</div>