<?php
        if ($row) {

        	$id_conference = $row->id_conference;
                $conference_name = $row->conference_name;
                $status = $row->status;
               // $submit_abstract = date("d-m-Y",strtotime($row->submit_abstract));
                $submit_abstract = $row->submit_abstract;
                $accept_abstract = date("d-m-Y",strtotime($row->accept_abstract));
                $submit_registrasi = date("d-m-Y",strtotime($row->submit_registrasi));
                $conference = date("d-m-Y",strtotime($row->conference));
                $submit_revised_paper = date("d-m-Y",strtotime($row->submit_revised_paper));
                $accept_revised = date("d/m/Y",strtotime($row->accept_revised));
                $payment_regist = date("d-m-Y",strtotime($row->payment_regist));
                $payment_paper = date("d-m-Y",strtotime($row->payment_paper));
                $action = "ubah";

        } else {

        	$id_conference = 
                $conference_name = 
                $status = 
                $submit_abstract = 
                $accept_abstract = 
                $submit_registrasi = 
                $conference = 
                $submit_revised_paper = 
                $accept_revised = 
                $payment_regist = 
                $payment_paper =  "";
        	$action = "simpan";

        }
        //echo $submit_abstract.$accept_revised;
?>
<script>
    $(document).ready(function(){
        $(".simpan").click(function(){
            $("#formsubmit").trigger("submit");
            return false;
        });
        $(".batal").click(function(){
            window.location = "<?php echo site_url('conference');?>";
            return false;
        });
        
    });
</script>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-success">
            <div class="box-header with-border">
            </div>
            <?php echo form_open('conference/simpan_conference/'.$action,array("id"=>"formsubmit"));?>
            <input type="hidden" name="idlama" value="<?php echo $id_conference; ?>" />
            <div class="box-body">
                <div class="form-horizontal">
                    <!-- <div class="form-group">
                        <label class="col-sm-2 control-label">Kode Negara</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" required name="kode" value="<?php echo $kode; ?>" <?php echo $readonly; ?>/>
                        </div>
                    </div> -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Conference Name</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" required name="conference_name" value="<?php echo $conference_name; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Submit Abstract</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="date" required name="submit_abstract" value="<?php echo $submit_abstract; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Accept Abstract</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="date" required name="accept_abstract" value="<?php echo $accept_abstract; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Submit Registration</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="date" required name="submit_registrasi" value="<?php echo $submit_registrasi; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Conference</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="date" required name="conference" value="<?php echo $conference; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Submit Revised Paper</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="date" required name="submit_revised_paper" value="<?php echo $submit_revised_paper; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Accept Revised</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="date" required name="accept_revised" value="<?php echo $accept_revised; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Payment Registration</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="date" required name="payment_regist" value="<?php echo $payment_regist; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Payment Paper</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="date" required name="payment_paper" value="<?php echo $payment_paper; ?>" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <div class="pull-right">
                    <div class="btn-group">
                       <button type=submit class="simpan btn btn-primary btn-md" title="Add"><i class="fa fa-save"></i></button>
                       <button class="batal btn btn-danger btn-md" title="Batal"><i class="fa fa-times-circle"></i></button>
                    </div>
                </div>
            </div>
            <?php echo form_close();?>
        </div>
    <div>
</div>