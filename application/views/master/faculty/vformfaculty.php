<?php
        if ($row) {

        	$id_fakultas = $row->id_fakultas;
            $fakultas_name = $row->fakultas_name;
            $id_universitas = $row->id_universitas;
            $action = "ubah";

        } else {

        	$id_fakultas = 
            $fakultas_name = 
            $id_universitas = "";
        	$action = "simpan";

        }
?>
<script>
    $(document).ready(function(){
        $(".simpan").click(function(){
            $("#formsubmit").trigger("submit");
            return false;
        });
        $(".batal").click(function(){
            window.location = "<?php echo site_url('faculty');?>";
            return false;
        });
    });
</script>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-success">
            <div class="box-header with-border">
            </div>
            <?php echo form_open('faculty/simpan_faculty/'.$action,array("id"=>"formsubmit"));?>
            <input type="hidden" name="idlama" value="<?php echo $id_fakultas; ?>" />
            <div class="box-body">
                <div class="form-horizontal">
                    <!-- <div class="form-group">
                        <label class="col-sm-2 control-label">Kode Negara</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" required name="kode" value="<?php echo $kode; ?>" <?php echo $readonly; ?>/>
                        </div>
                    </div> -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Faculty Name</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" required name="fakultas_name" value="<?php echo $fakultas_name; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Univercity</label>
                        <div class="col-sm-10">
                            <!-- <input class="form-control" type="text" required name="id_universitas" value="<?php echo $id_universitas; ?>" /> -->
                            <select name="id_universitas" class="form-control">
                                <?php
                                    foreach ($university->result() as $row1){
                                    if ($id_universitas == $row1->id_universitas) $selected = "selected"; else $selected = "";
                                        echo "<option value='".$row1->id_universitas."' ".$selected.">".$row1->universitas_name."</option>";
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="box-footer">
                <div class="pull-right">
                    <div class="btn-group">
                       <button type=submit class="simpan btn btn-primary btn-md" title="Add"><i class="fa fa-save"></i></button>
                       <button class="batal btn btn-danger btn-md" title="Batal"><i class="fa fa-times-circle"></i></button>
                    </div>
                </div>
            </div>
            <?php echo form_close();?>
        </div>
    <div>
</div>