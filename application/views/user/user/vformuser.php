<?php
        if ($row) {

        	$id_user = $row->id_user;
                $username = $row->username;
                $email = $row->email;
                $password = $row->password;
                $full_name = $row->full_name;
                $phone_number = $row->phone_number;
                $id_level = $row->id_level;
                //$gender = $row->gender;
                $man = ($row->gender == "Man") ? "selected" : "";
                $woman = ($row->gender == "Woman") ? "selected" : "";
                $foto = $row->foto;
                $action = "ubah";

        } else {

        	$id_user = 
                $username = 
                $email = 
                $password = 
                $full_name = 
                $phone_number = 
                $id_level = 
                $man = $woman =
                //$gender = 
                $foto =  "";
        	$action = "simpan";

        }
?>
<script>
    $(document).ready(function(){
        $(".simpan").click(function(){
            $("#formsubmit").trigger("submit");
            return false;
        });
        $(".batal").click(function(){
            window.location = "<?php echo site_url('user');?>";
            return false;
        });
        $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
            var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label; 
            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }
            
        });
    });
    $(document).on('change', '.btn-file :file', function() {
      var input = $(this),
          numFiles = input.get(0).files ? input.get(0).files.length : 1,
          label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
      input.trigger('fileselect', [numFiles, label]);
    });
</script>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-success">
            <div class="box-header with-border">
            </div>
            <?php echo form_open('user/simpan_user/'.$action,array("id"=>"formsubmit"));?>
            <input type="hidden" name="idlama" value="<?php echo $id_user; ?>" />
            <div class="box-body">
                <div class="form-horizontal">
                    <!-- <div class="form-group">
                        <label class="col-sm-2 control-label">Kode Negara</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" required name="kode" value="<?php echo $kode; ?>" <?php echo $readonly; ?>/>
                        </div>
                    </div> -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Username</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" required name="username" value="<?php echo $username; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" required name="email" value="<?php echo $email; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" required name="password" value="<?php echo $password; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Full Name</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" required name="full_name" value="<?php echo $full_name; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Phone Number</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" required name="phone_number" value="<?php echo $phone_number; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Level</label>
                        <div class="col-sm-10">
                            <select name="id_level" class="form-control">
                                <?php
                                    foreach ($level->result() as $row1){
                                    if ($id_level == $row1->id_level) $selected = "selected"; else $selected = "";
                                        echo "<option value='".$row1->id_level."' ".$selected.">".$row1->level_name."</option>";
                                    }
                                ?>
                            </select>
                            <!-- <input class="form-control" type="text" required name="id_level" value="<?php echo $id_level; ?>" /> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Gender</label>
                        <div class="col-sm-10">
                            <select name="gender" class="form-control">
                                <option value='Man' <?php echo $man;?>>Man</option>
                                <option value='Woman' <?php echo $woman;?>>Woman</option>
                            </select>
                            <!-- <input class="form-control" type="text" required name="gender" value="<?php echo $gender; ?>" /> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Foto</label>
                        <div class="col-sm-6">
                            <div class="input-group">         
                                <input type="text" class="form-control" readonly>        
                                <span class="input-group-btn">
                                    <span class="btn btn-warning btn-file"><i class="fa fa-folder-open"></i>&nbsp;&nbsp;Browse<input type="file" name="foto" class="form-control"></span>
                                </span>
                            </div> 

                            <!-- <input class="form-control" type="text" required name="foto" value="<?php echo $foto; ?>" /> -->
                        </div>
                        <div class="col-sm-4">
                            <div class="image">
                                <?php
                                if (empty($foto)){
                                    echo img(array("src"=>"assets/dist/img/avatar5.png","width"=>"150","class"=>"img-thumbnail"));
                                } else {
                                    echo img(array("src"=>"assets/upload/img/user/".$foto,"width"=>"150","class"=>"img-thumbnail"));
                                }
                                ?>
                                </div>
                        </div>
                       <!--  <div class="col-sm-10">
                            <input class="form-control" type="text" required name="foto" value="<?php echo $foto; ?>" />
                        </div> -->
                    </div>
                    

                </div>
            </div>
            <div class="box-footer">
                <div class="pull-right">
                    <div class="btn-group">
                       <button type=submit class="simpan btn btn-primary btn-md" title="Add"><i class="fa fa-save"></i></button>
                       <button class="batal btn btn-danger btn-md" title="Batal"><i class="fa fa-times-circle"></i></button>
                    </div>
                </div>
            </div>
            <?php echo form_close();?>
        </div>
    <div>
</div>