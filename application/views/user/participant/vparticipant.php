<script>
	$(document).ready(function() {
		// $('#myTable').fixedHeaderTable({ height: '500', altClass: 'odd', footer: true});
        $("table tr#data:first").addClass("bg-gray");
        $("table tr#data ").click(function(){
            $("table tr#data ").removeClass("bg-gray");
            $(this).addClass("bg-gray");
        });
		$(".simpan").click(function(){
			window.location="<?php echo site_url('participant/formparticipant');?>";
			return false;
		});
		$(".ubah").click(function(){
			var id= $(".bg-gray").attr("href");
			window.location="<?php echo site_url('participant/formparticipant');?>/"+id;
			return false;
		});
		$(".hapus").click(function(){
			var id=$(".bg-gray").attr("href");
			window.location="<?php echo site_url('participant/hapusparticipant');?>/"+id;
		});
	});
</script>
<?php
	if($this->session->flashdata('message')){
		$pesan=explode('-', $this->session->flashdata('message'));
		echo "<div class='alert alert-".$pesan[0]."' alert-dismissable>
		<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
		<b>".$pesan[1]."</b>
		</div>";
	}
?>
<div class="row">
	<div class="col-md-12">
		<div class="box box-success">
			<div class="box-header with-border">
			</div>
			<div class="box-body">
				<table id="myTable" class="table table-bordered table-hover">
					<thead>
						<tr class="bg-navy">
	                        <th width='10%'>No</th>
	                        <th width='15%'>Participant Code</th>
	                        <th width='40%'>Email</th>
	                        <th width='40%'>Full Name</th>
	                    </tr>
					</thead>
					<tbody>
						<?php
							$i = 0;
		                    foreach ($row->result() as $row){
		                        $i++;
		                        echo "<tr id='data' href='".$row->id_participant."'>
		        						 <td>".$i."</td>
		                                 <td align=center>".$row->no_participant."</td>
		                                 <td>".$row->email."</td>
		                                 <td>".$row->full_name."</td>
		                              </tr>";
		                    }
						?>
					</tbody>
				</table>
			</div>
			<div class="box-footer">
				<div class="btn-group pull-right">
                    <button class="simpan btn btn-primary"><i class="fa  fa-plus"></i></button>
                    <button class="ubah btn btn-warning"><i class="fa fa-pencil"></i></button>
                    <button class="hapus btn btn-danger" ><i class="fa  fa-times"></i></button>
				</div>
			</div>
		</div>
	</div>
</div>