<?php
        if ($row) {

        	$id_participant = $row->id_participant;
                $no_participant = $row->no_participant;
                $email = $row->email;
                $password = $row->password;
                $full_name = $row->full_name;
                $status = $row->status;
                $register_date = $row->register_date;
                $phone_number = $row->phone_number;
                //$gender = $row->gender;
                $man = ($row->gender == "Man") ? "selected" : "";
                $woman = ($row->gender == "Woman") ? "selected" : "";
                $presenter = $row->presenter;
                $address = $row->address;
                $id_country = $row->id_country;
                $id_city = $row->id_city;
                $zip_code = $row->zip_code;
                $pid_universitas= $row->id_universitas;
                $id_fakultas = $row->id_fakultas;
                $payment_regist = $row->payment_regist;
                $payment_date = $row->payment_date;
                $payment_date_upload = $row->payment_date_upload;
                $payment_status = $row->payment_status;
                $payment_file = $row->payment_file;
                $foto = $row->foto;
                $action = "ubah";

        } else {

        	$id_participant = 
                $no_participant = 
                $email = 
                $password = 
                $full_name = 
                $status = 
                $register_date = 
                $phone_number = 
                //$gender = 
                $man = $woman =
                $presenter = 
                $address = 
                $id_country = 
                $id_city = 
                $zip_code = 
                $id_universitas = 
                $id_fakultas = 
                $payment_regist = 
                $payment_date = 
                $payment_date_upload = 
                $payment_status = 
                $payment_file = 
                $foto =  "";
        	$action = "simpan";

        }
?>

<script>
    $(document).ready(function(){
        $(".simpan").click(function(){
            $("#formsubmit").trigger("submit");
            return false;
        });
        $(".batal").click(function(){
            window.location = "<?php echo site_url('participant');?>";
            return false;
        });
        $('.btn-file :file').on('fileselect', function(event, numFiles, label) {
            var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label; 
            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }
            
        });
    });
    $(document).on('change', '.btn-file :file', function() {
      var input = $(this),
          numFiles = input.get(0).files ? input.get(0).files.length : 1,
          label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
      input.trigger('fileselect', [numFiles, label]);
    });
</script>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-success">
            <div class="box-header with-border">
            </div>
            <?php echo form_open('participant/simpan_participant/'.$action,array("id"=>"formsubmit"));?>
            <input type="hidden" name="idlama" value="<?php echo $id_participant; ?>" />
            <div class="box-body">
                <div class="form-horizontal">
                    <!-- <div class="form-group">
                        <label class="col-sm-2 control-label">Kode Negara</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" required name="kode" value="<?php echo $kode; ?>" <?php echo $readonly; ?>/>
                        </div>
                    </div> -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">No Participant</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" required name="no_participant" value="<?php echo $no_participant; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" required name="email" value="<?php echo $email; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Full Name</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" required name="full_name" value="<?php echo $full_name; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Phone Number</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" required name="phone_number" value="<?php echo $phone_number; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Gender</label>
                        <div class="col-sm-10">
                            <select name="gender" class="form-control">
                                <option value='Man' <?php echo $man;?>>Man</option>
                                <option value='Woman' <?php echo $woman;?>>Woman</option>
                            </select>
                            <!-- <input class="form-control" type="text" required name="gender" value="<?php echo $gender; ?>" /> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Presenter</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" required name="presenter" value="<?php echo $presenter; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Address</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" required name="address" value="<?php echo $address; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Country</label>
                        <div class="col-sm-10">
                            <select name="id_country" class="form-control">
                                <?php
                                    foreach ($country->result() as $row1){
                                    if ($id_country == $row1->id_country) $selected = "selected"; else $selected = "";
                                        echo "<option value='".$row1->id_country."' ".$selected.">".$row1->country_name."</option>";
                                    }
                                ?>
                            </select>
                            <!-- <input class="form-control" type="text" required name="id_country" value="<?php echo $id_country; ?>" /> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">City</label>
                        <div class="col-sm-10">
                            <select name="id_city" class="form-control">
                                <?php
                                    foreach ($city->result() as $row1){
                                    if ($id_city == $row1->id_city) $selected = "selected"; else $selected = "";
                                        echo "<option value='".$row1->id_city."' ".$selected.">".$row1->city_name."</option>";
                                    }
                                ?>
                            </select>
                           <!--  <input class="form-control" type="text" required name="id_city" value="<?php echo $id_city; ?>" /> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Zip Code</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" required name="zip_code" value="<?php echo $zip_code; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">University</label>
                        <div class="col-sm-10">
                            <select name="id_universitas" class="form-control">
                                <?php
                                    foreach ($university->result() as $row1){
                                    if ($id_universitas == $row1->id_universitas) $selected = "selected"; else $selected = "";
                                        echo "<option value='".$row1->id_universitas."' ".$selected.">".$row1->universitas_name."</option>";
                                    }
                                ?>
                            </select>
                            <!-- <input class="form-control" type="text" required name="id_universitas" value="<?php echo $id_universitas; ?>" /> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Faculty</label>
                        <div class="col-sm-10">
                            <select name="id_fakultas" class="form-control">
                                <?php
                                    foreach ($faculty->result() as $row1){
                                    if ($id_fakultas == $row1->id_fakultas) $selected = "selected"; else $selected = "";
                                        echo "<option value='".$row1->id_fakultas."' ".$selected.">".$row1->fakultas_name."</option>";
                                    }
                                ?>
                            </select>
                            <!-- <input class="form-control" type="text" required name="id_fakultas" value="<?php echo $id_fakultas; ?>" /> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Payment Date</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="date" required name="payment_date" value="<?php echo $payment_date; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Payment Date Upload</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="date" required name="payment_date_upload" value="<?php echo $payment_date_upload; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Payment Status</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" required name="payment_status" value="<?php echo $payment_status; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Payment File</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="text" required name="payment_file" value="<?php echo $payment_file; ?>" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Foto</label>
                        <div class="col-sm-6">
                            <div class="input-group">         
                                <input type="text" class="form-control" readonly>        
                                <span class="input-group-btn">
                                    <span class="btn btn-warning btn-file"><i class="fa fa-folder-open"></i>&nbsp;&nbsp;Browse<input type="file" name="foto" class="form-control"></span>
                                </span>
                            </div> 

                            <!-- <input class="form-control" type="text" required name="foto" value="<?php echo $foto; ?>" /> -->
                        </div>
                        <div class="col-sm-4">
                            <div class="image">
                                <?php
                                if (empty($foto)){
                                    echo img(array("src"=>"assets/dist/img/avatar5.png","width"=>"150","class"=>"img-thumbnail"));
                                } else {
                                    echo img(array("src"=>"assets/upload/img/user/".$foto,"width"=>"150","class"=>"img-thumbnail"));
                                }
                                ?>
                                </div>
                        </div>
                        <!-- <div class="col-sm-10">
                            <input class="form-control" type="text" required name="foto" value="<?php echo $foto; ?>" />
                        </div> -->
                    </div>
                    
                </div>
            </div>
            <div class="box-footer">
                <div class="pull-right">
                    <div class="btn-group">
                       <button type=submit class="simpan btn btn-primary btn-md" title="Add"><i class="fa fa-save"></i></button>
                       <button class="batal btn btn-danger btn-md" title="Batal"><i class="fa fa-times-circle"></i></button>
                    </div>
                </div>
            </div>
            <?php echo form_close();?>
        </div>
    <div>
</div>