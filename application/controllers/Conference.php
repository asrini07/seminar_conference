<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Conference extends CI_Controller {
	function __construct() {
	    parent::__construct();
	    $this->load->Model('Mconference');
	    // if (($this->session->userdata('username') == NULL)||($this->session->userdata('password') == NULL))
	    // {
	    //        redirect("login/logout","refresh");
	    // }
	}
	
	public function index() {
	    $data["title"] = "Conference < Conference";
	    //$data["nama_user"] = $this->session->userdata('nama_user');
	    $data["menu"] = "master";
	    $data["content"] = "master/conference/vconference";
	    $data["judul"] = '<h1 class="no-margin-bottom">Conference</h1>';
	    $data["breadcrumb"] = '<li class="breadcrumb-item active">Conference</li>';
	    $cari = $this->input->post("cari");
    	$data["cari"] = $cari;
	    $data["row"] = $this->Mconference->getconference($cari);
	    $this->load->view('template',$data);
	}

	public function formconference($id = 0) {
	    $data["title"] = "Conference < Conference";
	    //$data["nama_user"] = $this->session->userdata('nama_user');
	    $data["menu"] = "master";
	    $data["content"] = "master/conference/vformconference";
	    $data["judul"] = '<h1 class="no-margin-bottom">Conference</h1>';
	    $data["breadcrumb"] = '<li class="breadcrumb-item active">Conference</li>';
    	$data["id"] = $id;
	    $data["row"] = $this->Mconference->getconferencedetail($id);
	    $this->load->view('template',$data);
	} 

	public function hapus_conference($id){
        $message = $this->Mcity->hapus_conference($id);
        $this->session->set_flashdata("message",$message);
        redirect("conference");
    }

	public function simpan_conference($action){
        $message = $this->Mconference->simpan_conference($action);
        $this->session->set_flashdata("message",$message);
        redirect("conference");
    } 


}
