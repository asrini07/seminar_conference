<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Country extends CI_Controller {
	function __construct() {
	    parent::__construct();
	    $this->load->Model('Mcountry');
	    // if (($this->session->userdata('username') == NULL)||($this->session->userdata('password') == NULL))
	    // {
	    //        redirect("login/logout","refresh");
	    // }
	}
	
	public function index() {
	    $data["title"] = "Conference < Country";
	    //$data["nama_user"] = $this->session->userdata('nama_user');
	    $data["menu"] = "master";
	    $data["content"] = "master/country/vcountry";
	    $data["judul"] = '<h1 class="no-margin-bottom">Country</h1>';
	    $data["breadcrumb"] = '<li class="breadcrumb-item active">Country</li>';
	    $cari = $this->input->post("cari");
    	$data["cari"] = $cari;
	    $data["row"] = $this->Mcountry->getcountry($cari);
	    $this->load->view('template',$data);
	}

	public function formcountry($id=0) {
		$data["title"] = "Conference < Country";
	    //$data["nama_user"] = $this->session->userdata('nama_user');
	    $data["menu"] = "master";
	    $data["content"] = "master/country/vformcountry";
	    $data["judul"] = '<h1 class="no-margin-bottom">Form Country</h1>';
	    $data["breadcrumb"] = '<li class="breadcrumb-item active">Country</li>';
	    $data["id"]=$id;
        $data["row"] = $this->Mcountry->getcountrydetail($id);
        $this->load->view('template',$data);
	}

    public function hapuscountry($action){
        $message = $this->Mcountry->hapuscountry($action);
        $this->session->set_flashdata("message",$message);
        redirect("country");
	}

	public function simpancountry($action){
        $message = $this->Mcountry->simpancountry($action);
        $this->session->set_flashdata("message",$message);
        redirect("country");
    }

}
