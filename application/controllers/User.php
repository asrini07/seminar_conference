<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	function __construct() {
	    parent::__construct();
	    $this->load->Model('Muser');
	    $this->load->Model('Mlevel');
	    // if (($this->session->userdata('username') == NULL)||($this->session->userdata('password') == NULL))
	    // {
	    //        redirect("login/logout","refresh");
	    // }
	}
	
	public function index() {
	    $data["title"] = "Conference < User";
	    //$data["nama_user"] = $this->session->userdata('nama_user');
	    $data["menu"] = "user";
	    $data["content"] = "user/user/vuser";
	    $data["judul"] = '<h1 class="no-margin-bottom">User</h1>';
	    $data["breadcrumb"] = '<li class="breadcrumb-item active">User</li>';
	    $cari = $this->input->post("cari");
    	$data["cari"] = $cari;
	    $data["row"] = $this->Muser->getuser($cari);
	    $this->load->view('template',$data);
	} 

	public function formuser($id = 0) {
	    $data["title"] = "Conference < User";
	    //$data["nama_user"] = $this->session->userdata('nama_user');
	    $data["menu"] = "user";
	    $data["content"] = "user/user/vformuser";
	    $data["judul"] = '<h1 class="no-margin-bottom">Form User</h1>';
	    $data["breadcrumb"] = '<li class="breadcrumb-item active">User</li>';
    	$data["id"] = $id;
	    $data["row"] = $this->Muser->getuserdetail($id);
	    $data["level"] = $this->Mlevel->getlevel("");
	    $this->load->view('template',$data);
	} 

	public function hapus_user($id){
        $message = $this->Mcity->hapus_user($id);
        $this->session->set_flashdata("message",$message);
        redirect("user");
    }

	public function simpan_user($action){
		$file_name=($action=="simpan") ? $this->input->post("username") : $this->input->post("username");
   		$config['allowed_types'] = 'jpg|jpeg|png|gif';
   		$config['overwrite']  = TRUE;
   		$config['remove_spaces']  = TRUE;
   		$foto = array();
   		if (!empty($_FILES['foto']['name'])){
     		$config['upload_path'] = './assets/upload/img/user/';
     		$config["file_name"] = $file_name;
     		$this->upload->initialize($config);
     		$this->upload->do_upload('foto');
     		$foto = $this->upload->data();
   		} 

        $message = $this->Muser->simpan_user($action,$foto);
        $this->session->set_flashdata("message",$message);
        redirect("user");
    }


}
