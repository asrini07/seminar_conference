<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Participant extends CI_Controller {
	function __construct() {
	    parent::__construct();
	    $this->load->Model('Mparticipant');
	    $this->load->Model('Mcountry');
	    $this->load->Model('Mcity');
	    $this->load->Model('Muniversity');
	    $this->load->Model('Mfaculty');
	    // if (($this->session->userdata('username') == NULL)||($this->session->userdata('password') == NULL))
	    // {
	    //        redirect("login/logout","refresh");
	    // }
	}
	
	public function index() {
	    $data["title"] = "Conference < Participant";
	    //$data["nama_user"] = $this->session->userdata('nama_user');
	    $data["menu"] = "user";
	    $data["content"] = "user/participant/vparticipant";
	    $data["judul"] = '<h1 class="no-margin-bottom">Participant</h1>';
	    $data["breadcrumb"] = '<li class="breadcrumb-item active">Participant</li>';
	    $cari = $this->input->post("cari");
    	$data["cari"] = $cari;
	    $data["row"] = $this->Mparticipant->getparticipant($cari);
	    $this->load->view('template',$data);
	} 

	public function formparticipant($id = 0) {
	    $data["title"] = "Conference < Participant";
	    //$data["nama_user"] = $this->session->userdata('nama_user');
	    $data["menu"] = "user";
	    $data["content"] = "user/participant/vformparticipant";
	    $data["judul"] = '<h1 class="no-margin-bottom">Form Participant</h1>';
	    $data["breadcrumb"] = '<li class="breadcrumb-item active">Participant</li>';
    	$data["id"] = $id;
	    $data["row"] = $this->Mparticipant->getparticipantdetail($id);
	    $data["country"] = $this->Mcountry->getcountry("");
	    $data["city"] = $this->Mcity->getcity("");
	    $data["faculty"] = $this->Mfaculty->getfaculty("");
	    $data["university"] = $this->Muniversity->getuniversity("");
	    $this->load->view('template',$data);
	} 

	public function hapus_participant($id){
        $message = $this->Mparticipant->hapus_participant($id);
        $this->session->set_flashdata("message",$message);
        redirect("participant");
    }

	public function simpan_participant($action){
		$file_name=($action=="simpan") ? $this->input->post("username") : $this->input->post("username");
   		$config['allowed_types'] = 'jpg|jpeg|png|gif';
   		$config['overwrite']  = TRUE;
   		$config['remove_spaces']  = TRUE;
   		$foto = array();
   		if (!empty($_FILES['foto']['name'])){
     		$config['upload_path'] = './assets/upload/img/user/';
     		$config["file_name"] = $file_name;
     		$this->upload->initialize($config);
     		$this->upload->do_upload('foto');
     		$foto = $this->upload->data();
   		} 

        $message = $this->Mparticipant->simpan_participant($action, $foto);
        $this->session->set_flashdata("message",$message);
        redirect("participant");
    }


}
