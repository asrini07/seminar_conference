<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faculty extends CI_Controller {
	function __construct() {
	    parent::__construct();
	    $this->load->Model('Mfaculty');
	    $this->load->Model('Muniversity');
	    // if (($this->session->userdata('username') == NULL)||($this->session->userdata('password') == NULL))
	    // {
	    //        redirect("login/logout","refresh");
	    // }
	}
	
	public function index() {
	    $data["title"] = "Conference < Faculty";
	    //$data["nama_user"] = $this->session->userdata('nama_user');
	    $data["menu"] = "master";
	    $data["content"] = "master/faculty/vfaculty";
	    $data["judul"] = '<h1 class="no-margin-bottom">Faculty</h1>';
	    $data["breadcrumb"] = '<li class="breadcrumb-item active">Faculty</li>';
	    $cari = $this->input->post("cari");
    	$data["cari"] = $cari;
	    $data["row"] = $this->Mfaculty->getfaculty($cari);
	    $this->load->view('template',$data);
	} 

	public function formfaculty($id=0) {
		$data["title"] = "Conference < Faculty";
	    //$data["nama_user"] = $this->session->userdata('nama_user');
	    $data["menu"] = "master";
	    $data["content"] = "master/faculty/vformfaculty";
	    $data["judul"] = '<h1 class="no-margin-bottom">Form Faculty</h1>';
	    $data["breadcrumb"] = '<li class="breadcrumb-item active">Faculty</li>';
	    $data["id"]=$id;
        $data["row"] = $this->Mfaculty->getfacultydetail($id);
        $data["university"] = $this->Muniversity->getuniversity("");
        $this->load->view('template',$data);
	}

    public function hapusfaculty($action){
        $message = $this->Mfaculty->hapusfaculty($action);
        $this->session->set_flashdata("message",$message);
        redirect("faculty");
	}

	public function simpan_faculty($action){
        $message = $this->Mfaculty->simpan_faculty($action);
        $this->session->set_flashdata("message",$message);
        redirect("faculty");
    }


}
