<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class University extends CI_Controller {
	function __construct() {
	    parent::__construct();
	    $this->load->Model('Muniversity');
	    $this->load->Model('Mcity');
	    // if (($this->session->userdata('username') == NULL)||($this->session->userdata('password') == NULL))
	    // {
	    //        redirect("login/logout","refresh");
	    // }
	}
	
	public function index() {
	    $data["title"] = "Conference < University";
	    //$data["nama_user"] = $this->session->userdata('nama_user');
	    $data["menu"] = "master";
	    $data["content"] = "master/university/vuniversity";
	    $data["judul"] = '<h1 class="no-margin-bottom">University</h1>';
	    $data["breadcrumb"] = '<li class="breadcrumb-item active">University</li>';
	    $cari = $this->input->post("cari");
    	$data["cari"] = $cari;
	    $data["row"] = $this->Muniversity->getuniversity($cari);
	    $this->load->view('template',$data);
	} 

	public function formuniversity($id=0) {
		$data["title"] = "Conference < University";
	    //$data["nama_user"] = $this->session->userdata('nama_user');
	    $data["menu"] = "master";
	    $data["content"] = "master/university/vformuniversity";
	    $data["judul"] = '<h1 class="no-margin-bottom">Form University</h1>';
	    $data["breadcrumb"] = '<li class="breadcrumb-item active">University</li>';
	    $data["id"]=$id;
        $data["row"] = $this->Muniversity->getuniversitydetail($id);
        $data["city"] = $this->Mcity->getcity("");
        $this->load->view('template',$data);
	}

    public function hapusuniversity($action){
        $message = $this->Muniversity->hapusuniversity($action);
        $this->session->set_flashdata("message",$message);
        redirect("university");
	}

	public function simpan_university($action){
        $message = $this->Muniversity->simpan_university($action);
        $this->session->set_flashdata("message",$message);
        redirect("university");
    }


}
