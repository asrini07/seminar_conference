<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Topic extends CI_Controller {
	function __construct() {
	    parent::__construct();
	    $this->load->Model('Mtopic');
	    // if (($this->session->userdata('username') == NULL)||($this->session->userdata('password') == NULL))
	    // {
	    //        redirect("login/logout","refresh");
	    // }
	}
	
	public function index() {
	    $data["title"] = "Conference < Topic";
	    //$data["nama_user"] = $this->session->userdata('nama_user');
	    $data["menu"] = "master";
	    $data["content"] = "master/topic/vtopic";
	    $data["judul"] = '<h1 class="no-margin-bottom">Topic</h1>';
	    $data["breadcrumb"] = '<li class="breadcrumb-item active">Topic</li>';
	    $cari = $this->input->post("cari");
    	$data["cari"] = $cari;
	    $data["row"] = $this->Mtopic->gettopic($cari);
	    $this->load->view('template',$data);
	} 

	public function formtopic($id=0) {
		$data["title"] = "Conference < Topic";
	    //$data["nama_user"] = $this->session->userdata('nama_user');
	    $data["menu"] = "master";
	    $data["content"] = "master/topic/vformtopic";
	    $data["judul"] = '<h1 class="no-margin-bottom">Form Topic</h1>';
	    $data["breadcrumb"] = '<li class="breadcrumb-item active">Topic</li>';
	    $data["id"]=$id;
        $data["row"] = $this->Mtopic->gettopicdetail($id);
        $this->load->view('template',$data);
	}

    public function hapustopic($action){
        $message = $this->Mtopic->hapustopic($action);
        $this->session->set_flashdata("message",$message);
        redirect("topic");
	}

	public function simpan_topic($action){
        $message = $this->Mtopic->simpan_topic($action);
        $this->session->set_flashdata("message",$message);
        redirect("topic");
    }


}
