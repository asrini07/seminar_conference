<?php
	class Mcity extends CI_Model {

		function __construct() {
	        parent::__construct();
	   	}
	   	
	   	function getcity($cari) {
	   		if ($cari != "") {
				$this->db->like("id_city",$cari);
				$this->db->or_like("city_name",$cari);
			}

			$this->db->select('city.*, country.country_name');
	   		$this->db->join('country', 'country.id_country=city.id_country','LEFT');
	   		$q = $this->db->get('city');
	   		return $q;
	   	}

	   	function getcitydetail($id) {
	   		$this->db->where('id_city',$id);
	   		$q = $this->db->get('city');
	   		return $q->row();
	   	}

	   	function simpan_city($aksi) {
	   		$data = array(
	   					//'id_city'	 => $this->input->post('id_city'),
	   					'city_name'  => $this->input->post('city_name'), 
	   					'id_country'  => $this->input->post('id_country'), 
			);

			switch ($aksi) {
				case 'simpan':
					$this->db->insert('city', $data);
					break;
				case 'ubah':
					$this->db->where('id_city', $this->input->post('idlama'));
					$this->db->update('city', $data);
					break;
			}
			
			return "success-Data City berhasil di simpan";
	   	}

	   	function hapuscity($id) {
	   		$this->db->where('id_city',$id);
	   		$this->db->delete('city');
	   		return "danger-Data City berhasil di hapus";
	   	}
	}
?>