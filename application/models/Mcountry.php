<?php
	class Mcountry extends CI_Model {

		function __construct() {
	        parent::__construct();
	   	}
	   	
	   	function getcountry($cari) {
	   		if ($cari != "") {
				$this->db->like("id_country",$cari);
				$this->db->or_like("country_name",$cari);
			}

	   		$q = $this->db->get('country');
	   		return $q;
	   	}

	   	function getcountrydetail($id) {
	   		$this->db->where('id_country',$id);
	   		$q = $this->db->get('country');
	   		return $q->row();
	   	}

	   	function simpancountry($aksi) {
	   		$data = array(
	   					//'id_country'	 => $this->input->post('id_country'),
	   					'country_name'  => $this->input->post('country_name'), 
			);

			switch ($aksi) {
				case 'simpan':
					$this->db->insert('country', $data);
					break;
				case 'ubah':
					$this->db->where('id_country', $this->input->post('idlama'));
					$this->db->update('country', $data);
					break;
			}

			return "success-Data Country berhasil di simpan";
	   	}

	   	function hapuscountry($id) {
	   		$this->db->where('id_country',$id);
	   		$this->db->delete('country');
	   		return "danger-Data Country berhasil di hapus";
	   	}
	}
?>