<?php
	class Muser extends CI_Model {

		function __construct() {
	        parent::__construct();
	   	}
	   	
	   	function getuser($cari) {
	   		if ($cari != "") {
				$this->db->like("id_user",$cari);
				$this->db->or_like("user_name",$cari);
			}

	   		$q = $this->db->get('user');
	   		return $q;
	   	}

	   	function getuserdetail($id) {
	   		$this->db->where('id_user',$id);
	   		$q = $this->db->get('user');
	   		return $q->row();
	   	}

	   	function simpan_user($aksi, $foto) {
	   		$data = array(
	   					'id_user'	 => $this->input->post('id_user'),
	   					'username'  => $this->input->post('username'), 
	   					'email'  => $this->input->post('email'), 
	   					'password'  => $this->input->post('password'), 
	   					'full_name'  => $this->input->post('full_name'), 
	   					'phone_number'  => $this->input->post('phone_number'), 
	   					'id_level'  => $this->input->post('id_level'), 
	   					'gender'  => $this->input->post('gender'), 
			);
			if (!empty($foto)) {
   				$f = array('foto' => $foto["file_name"]);
   				$data = array_merge($data,$f);
   				$d = $this->db->get_where("user",array('id_user' => $this->input->post('idlama')))->row()->foto;
   			}

			switch ($aksi) {
				case 'simpan':
					$this->db->insert('user', $data);
					break;
				case 'ubah':
					$this->db->where('id_user', $this->input->post('idlama'));
					$this->db->update('user', $data);
					break;
			}
			
			return "success-Data Country berhasil di simpan";
	   	}

	   	function hapususer($id) {
	   		$this->db->where('id_user',$id);
	   		$this->db->delete('user');
	   		return "danger-Data Country berhasil di hapus";
	   	}
	}
?>