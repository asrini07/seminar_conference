<?php
	class Mparticipant extends CI_Model {

		function __construct() {
	        parent::__construct();
	   	}
	   	
	   	function getparticipant($cari) {
	   		if ($cari != "") {
				$this->db->like("id_participant",$cari);
				$this->db->or_like("participant_name",$cari);
			}

	   		$q = $this->db->get('participant');
	   		return $q;
	   	}

	   	function getparticipantdetail($id) {
	   		$this->db->where('id_participant',$id);
	   		$q = $this->db->get('participant');
	   		return $q->row();
	   	}

	   	function simpan_participant($aksi, $foto) {
	   		$data = array(
	   					'id_participant'	 => $this->input->post('id_participant'),
	   					'no_participant'  => $this->input->post('no_participant'), 
	   					'email'  => $this->input->post('email'), 
	   					//'password'  => $this->input->post('password'), 
	   					'full_name'  => $this->input->post('full_name'), 
	   					//'status'  => $this->input->post('status'), 
	   					//'register_date'  => $this->input->post('register_date'), 
	   					'phone_number'  => $this->input->post('phone_number'), 
	   					'gender'  => $this->input->post('gender'), 
	   					'presenter'  => $this->input->post('presenter'), 
	   					'address'  => $this->input->post('address'),  
	   					'id_country'  => $this->input->post('id_country'), 
	   					'id_city'  => $this->input->post('id_city'), 
	   					'zip_code'  => $this->input->post('zip_code'),  
	   					'id_universitas'  => $this->input->post('id_universitas'), 
	   					'id_fakultas'  => $this->input->post('id_fakultas'), 
	   					//'payment_regist'  => $this->input->post('payment_regist'),  
	   					'payment_date'  => $this->input->post('payment_date'), 
	   					'payment_date_upload'  => $this->input->post('payment_date_upload'), 
	   					'payment_status'  => $this->input->post('payment_status'),  
	   					'payment_file'  => $this->input->post('payment_file'), 
			);
			if (!empty($foto)) {
   				$f = array('foto' => $foto["file_name"]);
   				$data = array_merge($data,$f);
   				$d = $this->db->get_where("participant",array('id_participant' => $this->input->post('idlama')))->row()->foto;
   			}

			switch ($aksi) {
				case 'simpan':
					$this->db->insert('participant', $data);
					break;
				case 'ubah':
					$this->db->where('id_participant', $this->input->post('idlama'));
					$this->db->update('participant', $data);
					break;
			}
			
			return "success-Data Participant berhasil di simpan";
	   	}

	   	function hapusparticipant($id) {
	   		$this->db->where('id_participant',$id);
	   		$this->db->delete('participant');
	   		return "danger-Data Participant berhasil di hapus";
	   	}
	}
?>