<?php
	class Muniversity extends CI_Model {

		function __construct() {
	        parent::__construct();
	   	}
	   	
	   	function getuniversity($cari) {
	   		if ($cari != "") {
				$this->db->like("id_universitas",$cari);
				$this->db->or_like("universitas_name",$cari);
			}

	   		$q = $this->db->get('universitas');
	   		return $q;
	   	}

	   	function getuniversitydetail($id) {
	   		$this->db->where('id_universitas',$id);
	   		$q = $this->db->get('universitas');
	   		return $q->row();
	   	}

	   	function simpan_university($aksi) {
	   		$data = array(
	   					//'id_universitas'	 => $this->input->post('id_universitas'),
	   					'universitas_name'  => $this->input->post('universitas_name'), 
	   					'singkatan'  => $this->input->post('singkatan'), 
	   					'id_city'  => $this->input->post('id_city'), 
			);

			switch ($aksi) {
				case 'simpan':
					$this->db->insert('universitas', $data);
					break;
				case 'ubah':
					$this->db->where('id_universitas', $this->input->post('idlama'));
					$this->db->update('universitas', $data);
					break;
			}
			
			return "success-Data University berhasil di simpan";
	   	}

	   	function hapusuniversity($id) {
	   		$this->db->where('id_universitas',$id);
	   		$this->db->delete('universitas');
	   		return "danger-Data University berhasil di hapus";
	   	}
	}
?>