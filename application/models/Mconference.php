<?php
	class Mconference extends CI_Model {

		function __construct() {
	        parent::__construct();
	   	}
	   	
	   	function getconference($cari) {
	   		if ($cari != "") {
				$this->db->like("id_conference",$cari);
				$this->db->or_like("conference_name",$cari);
			}

	   		$q = $this->db->get('conference');
	   		return $q;
	   	}

	   	function getconferencedetail($id) {
	   		$this->db->where('id_conference',$id);
	   		$q = $this->db->get('conference');
	   		return $q->row();
	   	}

	   	function simpan_conference($aksi) {
	   		$data = array(
	   					'id_conference'	 => $this->input->post('id_conference'),
	   					'conference_name'  => $this->input->post('conference_name'), 
	   					'status'  => 0, 
	   					'submit_abstract'  => $this->input->post('submit_abstract'), 
	   					'accept_abstract'  => $this->input->post('accept_abstract'), 
	   					'submit_registrasi'  => $this->input->post('submit_registrasi'), 
	   					'conference'  => $this->input->post('conference'), 
	   					'submit_revised_paper'  => $this->input->post('submit_revised_paper'), 
	   					'accept_revised'  => $this->input->post('accept_revised'), 
	   					'payment_regist'  => $this->input->post('payment_regist'), 
	   					'payment_paper'  => $this->input->post('payment_paper'),  
			);

			switch ($aksi) {
				case 'simpan':
					$this->db->insert('conference', $data);
					break;
				case 'ubah':
					$this->db->where('id_conference', $this->input->post('idlama'));
					$this->db->update('conference', $data);
					break;
			}
			
			return "success-Data Country berhasil di simpan";
	   	}

	   	function hapusconference($id) {
	   		$this->db->where('id_conference',$id);
	   		$this->db->delete('conference');
	   		return "danger-Data Country berhasil di hapus";
	   	}
	}
?>