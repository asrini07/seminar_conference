<?php
	class Mtopic extends CI_Model {

		function __construct() {
	        parent::__construct();
	   	}
	   	
	   	function gettopic($cari) {
	   		if ($cari != "") {
				$this->db->like("id_topic",$cari);
				$this->db->or_like("topic_name",$cari);
			}

	   		$q = $this->db->get('topic');
	   		return $q;
	   	}

	   	function gettopicdetail($id) {
	   		$this->db->where('id_topic',$id);
	   		$q = $this->db->get('topic');
	   		return $q->row();
	   	}

	   	function simpan_topic($aksi) {
	   		$data = array(
	   					//'id_topic'	 => $this->input->post('id_topic'),
	   					'topic_name'  => $this->input->post('topic_name'), 
			);

			switch ($aksi) {
				case 'simpan':
					$this->db->insert('topic', $data);
					break;
				case 'ubah':
					$this->db->where('id_topic', $this->input->post('idlama'));
					$this->db->update('topic', $data);
					break;
			}
			
			return "success-Data Topic berhasil di simpan";
	   	}

	   	function hapustopic($id) {
	   		$this->db->where('id_topic',$id);
	   		$this->db->delete('topic');
	   		return "danger-Data Topic berhasil di hapus";
	   	}
	}
?>