-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Apr 21, 2019 at 06:01 PM
-- Server version: 5.7.21
-- PHP Version: 7.1.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `seminar`
--

-- --------------------------------------------------------

--
-- Table structure for table `abstract`
--

CREATE TABLE `abstract` (
  `id_abstract` int(11) NOT NULL,
  `id_paper` varchar(30) NOT NULL,
  `submited_by_1` varchar(30) NOT NULL,
  `submited_by_2` varchar(30) NOT NULL,
  `status_1` varchar(30) NOT NULL,
  `status_2` varchar(30) NOT NULL,
  `path_1` varchar(100) NOT NULL,
  `path_2` varchar(100) NOT NULL,
  `date_submit_1` datetime NOT NULL,
  `date_submit_2` datetime NOT NULL,
  `rejected_by` varchar(30) NOT NULL,
  `accepted_by` varchar(30) NOT NULL,
  `status_abctract` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id_city` int(11) NOT NULL,
  `city_name` varchar(50) NOT NULL,
  `id_country` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id_city`, `city_name`, `id_country`) VALUES
(1, 'Jakarta', '1'),
(2, 'Cirebon', '1'),
(3, 'Gaza', '6'),
(4, 'Mekah', '5'),
(5, 'Madinah', '5'),
(6, 'Jeddah', '5');

-- --------------------------------------------------------

--
-- Table structure for table `coment`
--

CREATE TABLE `coment` (
  `id_coment` int(11) NOT NULL,
  `sender` varchar(30) NOT NULL,
  `receiver` varchar(30) NOT NULL,
  `text` text NOT NULL,
  `file` varchar(100) NOT NULL,
  `date` datetime NOT NULL,
  `status_send` char(5) NOT NULL,
  `status_read` char(5) NOT NULL,
  `type` varchar(20) NOT NULL,
  `id_type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `conference`
--

CREATE TABLE `conference` (
  `id_conference` int(11) NOT NULL,
  `conference_name` varchar(200) NOT NULL,
  `status` varchar(10) NOT NULL,
  `submit_abstract` datetime NOT NULL,
  `accept_abstract` datetime NOT NULL,
  `submit_registrasi` datetime NOT NULL,
  `conference` datetime NOT NULL,
  `submit_revised_paper` datetime NOT NULL,
  `accept_revised` datetime NOT NULL,
  `payment_regist` double NOT NULL,
  `payment_paper` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `conference`
--

INSERT INTO `conference` (`id_conference`, `conference_name`, `status`, `submit_abstract`, `accept_abstract`, `submit_registrasi`, `conference`, `submit_revised_paper`, `accept_revised`, `payment_regist`, `payment_paper`) VALUES
(1, 'INTERNASIONAL SEMINAR', '0', '2019-04-01 00:00:00', '2019-04-03 00:00:00', '2019-04-05 00:00:00', '2019-04-07 00:00:00', '2019-04-09 00:00:00', '2019-04-11 00:00:00', 2019, 2019),
(2, 'CHALENGE OF INDUSTRIAL', '0', '2019-04-11 00:00:00', '2019-04-13 00:00:00', '2019-04-15 00:00:00', '2019-04-17 00:00:00', '2019-04-19 00:00:00', '2019-04-21 00:00:00', 2019, 2019);

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `id_config` int(11) NOT NULL,
  `universitas` varchar(50) NOT NULL,
  `address` text NOT NULL,
  `logo` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id_country` int(11) NOT NULL,
  `country_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id_country`, `country_name`) VALUES
(1, 'Indonesiaa'),
(2, 'Malaysia'),
(3, 'Singapura'),
(4, 'Turki'),
(5, 'Arab Saudi'),
(6, 'Palestina');

-- --------------------------------------------------------

--
-- Table structure for table `fakultas`
--

CREATE TABLE `fakultas` (
  `id_fakultas` int(11) NOT NULL,
  `fakultas_name` varchar(100) NOT NULL,
  `id_universitas` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fakultas`
--

INSERT INTO `fakultas` (`id_fakultas`, `fakultas_name`, `id_universitas`) VALUES
(1, 'Fakultas Teknik', '1'),
(2, 'Fakultas Ekonomi', '1'),
(3, 'Fakultas Teknik', '2');

-- --------------------------------------------------------

--
-- Table structure for table `full_paper`
--

CREATE TABLE `full_paper` (
  `id_full_paper` int(11) NOT NULL,
  `id_paper` varchar(30) NOT NULL,
  `submited_by_1` varchar(30) NOT NULL,
  `submited_by_2` varchar(30) NOT NULL,
  `status_1` varchar(30) NOT NULL,
  `status_2` varchar(30) NOT NULL,
  `path_1` varchar(100) NOT NULL,
  `path_2` varchar(100) NOT NULL,
  `date_submit_1` datetime NOT NULL,
  `date_submit_2` datetime NOT NULL,
  `rejected_by` varchar(30) NOT NULL,
  `accepted_by` varchar(30) NOT NULL,
  `status_full_paper` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id_level` int(11) NOT NULL,
  `level_name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id_level`, `level_name`) VALUES
(1, 'Super Admin'),
(2, 'Admin'),
(3, 'Reviewer'),
(4, 'Keuangan');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `id_log` int(11) NOT NULL,
  `type` varchar(30) NOT NULL,
  `id_user` varchar(30) NOT NULL,
  `activity` text NOT NULL,
  `datetime` datetime NOT NULL,
  `action` varchar(50) NOT NULL,
  `modul` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `paper`
--

CREATE TABLE `paper` (
  `id_paper` int(11) NOT NULL,
  `title` text NOT NULL,
  `id_owner` varchar(30) NOT NULL,
  `id_topic` varchar(30) NOT NULL,
  `writers` varchar(100) NOT NULL,
  `id_conference` varchar(30) NOT NULL,
  `total_payment` double NOT NULL,
  `status_payment` varchar(30) NOT NULL,
  `date_payment` datetime NOT NULL,
  `date_upload_payment` datetime NOT NULL,
  `file_payment` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `participant`
--

CREATE TABLE `participant` (
  `id_participant` int(11) NOT NULL,
  `no_participant` varchar(50) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(50) NOT NULL,
  `full_name` varchar(200) NOT NULL,
  `status` char(5) NOT NULL,
  `register_date` datetime NOT NULL,
  `phone_number` varchar(20) NOT NULL,
  `gender` char(5) NOT NULL,
  `presenter` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `id_country` varchar(20) NOT NULL,
  `id_city` varchar(20) NOT NULL,
  `zip_code` varchar(20) NOT NULL,
  `id_universitas` varchar(20) NOT NULL,
  `id_fakultas` varchar(20) NOT NULL,
  `payment_regist` double NOT NULL,
  `payment_date` datetime NOT NULL,
  `payment_date_upload` datetime NOT NULL,
  `payment_status` char(10) NOT NULL,
  `payment_file` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `participant`
--

INSERT INTO `participant` (`id_participant`, `no_participant`, `email`, `password`, `full_name`, `status`, `register_date`, `phone_number`, `gender`, `presenter`, `address`, `id_country`, `id_city`, `zip_code`, `id_universitas`, `id_fakultas`, `payment_regist`, `payment_date`, `payment_date_upload`, `payment_status`, `payment_file`, `foto`) VALUES
(1, '123', 'asrini@indosystem.com', '', 'Aas Asrini', '', '0000-00-00 00:00:00', '085294901108', 'Man', '1', 'asasa', '1', '1', 'sasas', '1', '1', 0, '2019-04-15 00:00:00', '2019-04-21 00:00:00', '1', '1', '');

-- --------------------------------------------------------

--
-- Table structure for table `topic`
--

CREATE TABLE `topic` (
  `id_topic` int(11) NOT NULL,
  `topic_name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `topic`
--

INSERT INTO `topic` (`id_topic`, `topic_name`) VALUES
(1, 'Economicc'),
(2, 'Medicine');

-- --------------------------------------------------------

--
-- Table structure for table `universitas`
--

CREATE TABLE `universitas` (
  `id_universitas` int(11) NOT NULL,
  `universitas_name` varchar(50) NOT NULL,
  `singkatan` varchar(30) NOT NULL,
  `id_city` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `universitas`
--

INSERT INTO `universitas` (`id_universitas`, `universitas_name`, `singkatan`, `id_city`) VALUES
(1, 'Universitas Muhammadiyah Cirebon', 'UMC', '2'),
(2, 'Institut Teknologi Bandung', 'ITB', '1');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `phone_number` varchar(12) NOT NULL,
  `id_level` varchar(5) NOT NULL,
  `gender` char(5) NOT NULL,
  `foto` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `email`, `password`, `full_name`, `phone_number`, `id_level`, `gender`, `foto`) VALUES
(0, 'asrini07', 'asrini@indosystem.com', '123', 'Aas Asrini', '085294901108', '1', 'Man', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abstract`
--
ALTER TABLE `abstract`
  ADD PRIMARY KEY (`id_abstract`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id_city`);

--
-- Indexes for table `coment`
--
ALTER TABLE `coment`
  ADD PRIMARY KEY (`id_coment`);

--
-- Indexes for table `conference`
--
ALTER TABLE `conference`
  ADD PRIMARY KEY (`id_conference`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`id_config`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id_country`);

--
-- Indexes for table `fakultas`
--
ALTER TABLE `fakultas`
  ADD PRIMARY KEY (`id_fakultas`);

--
-- Indexes for table `full_paper`
--
ALTER TABLE `full_paper`
  ADD PRIMARY KEY (`id_full_paper`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id_log`);

--
-- Indexes for table `paper`
--
ALTER TABLE `paper`
  ADD PRIMARY KEY (`id_paper`);

--
-- Indexes for table `participant`
--
ALTER TABLE `participant`
  ADD PRIMARY KEY (`id_participant`),
  ADD KEY `no_member` (`no_participant`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `topic`
--
ALTER TABLE `topic`
  ADD PRIMARY KEY (`id_topic`);

--
-- Indexes for table `universitas`
--
ALTER TABLE `universitas`
  ADD PRIMARY KEY (`id_universitas`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `username` (`username`),
  ADD KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abstract`
--
ALTER TABLE `abstract`
  MODIFY `id_abstract` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id_city` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `coment`
--
ALTER TABLE `coment`
  MODIFY `id_coment` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `conference`
--
ALTER TABLE `conference`
  MODIFY `id_conference` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `config`
--
ALTER TABLE `config`
  MODIFY `id_config` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id_country` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `fakultas`
--
ALTER TABLE `fakultas`
  MODIFY `id_fakultas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `full_paper`
--
ALTER TABLE `full_paper`
  MODIFY `id_full_paper` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id_level` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `id_log` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `paper`
--
ALTER TABLE `paper`
  MODIFY `id_paper` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `participant`
--
ALTER TABLE `participant`
  MODIFY `id_participant` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `topic`
--
ALTER TABLE `topic`
  MODIFY `id_topic` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `universitas`
--
ALTER TABLE `universitas`
  MODIFY `id_universitas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
